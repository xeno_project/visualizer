local map = {
    startPoint = {x=-10,y=0,z=10,direction="left",orientation="west"},
    control = {player = true,camera = true},
    compass = true,
    grading = "",
    models = {},
    teleporters = {},
    doors = {},    
    camera = {
        y=4,
        distance=5,
        pitch=math.rad(-30)
    },    

    props = {},
}

-- /!\ Above code shouldn't be messed with

-- Start of block ***************************
-- Map element declaration
map.models[1] = {
    objectName = "sr-1-battlemap-floor_1.obj",
    textureName = "sr-1-battlemap-floor_1.png",
    culling = "back",
    position = {x=0,y=0,z=0},
    rotation = {math.rad(90), 0, 0},
    scale = {0.01,0.01,0.01},    
    opacity = 1
}
-- End of block*******************************

-- Start of block ***************************
-- Map element declaration
map.models[1] = {
    objectName = "sr-1-battlemap-floor_1.obj",
    textureName = "sr-1-battlemap-floor_1.png",
    culling = "back",
    position = {x=0,y=0,z=0},
    rotation = {math.rad(90), 0, 0},
    scale = {0.01,0.01,0.01},    
    opacity = 1
}
-- End of block*******************************

-- Start of block ***************************
-- Map element declaration
map.models[2] = {
    objectName = "sr-1-battlemap-walls_1.obj",
    textureName = "sr-1-battlemap-walls_1.png",
    culling = "back",
    position = {x=0,y=0,z=0},
    rotation = {math.rad(90), 0, 0},
    scale = {0.01,0.01,0.01},    
    opacity = 1
}
-- End of block*******************************

-- Start of block ***************************
-- Map element declaration
map.models[3] = {
    objectName = "sr-1-battlemap-pillars_1.obj",
    textureName = "sr-1-battlemap-pillars_1.png",
    culling = "back",
    position = {x=0,y=0,z=0},
    rotation = {math.rad(90), 0, 0},
    scale = {0.01,0.01,0.01},    
    opacity = 1
}
-- End of block*******************************

-- Start of block ***************************
-- Map element declaration
map.models[4] = {
    objectName = "sr-1-battlemap-railway_1.obj",
    textureName = "sr-1-battlemap-railway_1.png",
    culling = "back",
    position = {x=0,y=0,z=0},
    rotation = {math.rad(90), 0, 0},
    scale = {0.01,0.01,0.01},    
    opacity = 1
}
-- End of block*******************************

-- Start of block ***************************
-- Map element declaration
map.models[5] = {
    objectName = "sr-1-battlemap-boxes_1.obj",
    textureName = "sr-1-battlemap-boxes_1.png",
    culling = "back",
    position = {x=0,y=0,z=0},
    rotation = {math.rad(90), 0, 0},
    scale = {0.01,0.01,0.01},    
    opacity = 1
}
-- End of block*******************************

-- /!\ Below code shouldn't be messed with
map.elements = #map.models

return map
