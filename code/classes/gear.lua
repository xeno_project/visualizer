-- *************************************
-- *                                   *
-- *               Gear                *
-- *                             Class *
-- *************************************
gearClass = {};function gearClass:new(o)
	
    -- Constructor
	local function constructor(o)
	
		o = o or {}
		
		-- Base properties
		o.defaultName = "No name"
		o.GEAR_POSITION = {-70, 70, 0}
		o.GEAR_SCALE = {0.01, 0.01, 0.01}
		o.GEAR_ROTATION = {math.rad(90),math.rad(0),math.rad(-90)}
		
		-- Nested properties  
		o.model = {}
		o.joints = {}
		o.position = {
			x=0,
			y=0,
			z=0
		}
		
		-- Set metatable
		setmetatable(o, self)    
		self.__index = self

		
		return self

	end;self = constructor(o)

	-- *************************************
	-- *              Methods              *
	-- *************************************	
	-- Initialize gear
	function self:init(objectName, textureName, folder)
	
		-- Load gear from file
		self:load(objectName, textureName, folder)

	end

	-- 
	function self:load(objectName, textureName, folder)

		-- If distributed, load from external directory
		if love.filesystem.isFused() then

			-- Get project folder path
			local dir = love.filesystem.getSourceBaseDirectory()

			-- Mount Assets directory to load gear from it
			local success = love.filesystem.mount(dir, folder)

			-- If mount successful
			if success then
			
				-- Load the gear model
				local gearTexture = love.graphics.newImage("Assets/gear/"..textureName)

				-- Filter gear texture to pixellize
				gearTexture:setFilter("nearest", "nearest", 1)

				-- Create 3D model
				self.model = self.lib.newModel("Assets/gear/"..objectName, gearTexture, 
					self.GEAR_POSITION, 
					self.GEAR_ROTATION, 
					self.GEAR_SCALE
				)

			end

		-- Else load from default folder
		else

			-- load the gear model
			local gearTexture = love.graphics.newImage("assets/gear/"..textureName)

			-- Filter gear texture to pixellize
			gearTexture:setFilter("nearest", "nearest", 1)

			-- Create 3D model
			self.model = self.lib.newModel("assets/gear/"..objectName, gearTexture, 
				self.GEAR_POSITION, 
				self.GEAR_ROTATION, 
				self.GEAR_SCALE
			)
			
		end	

	end

	-- Render gear
	function self:render()

		-- Set gear culling
		love.graphics.setMeshCullMode("none")

		-- Draw gear
		self.model:draw()

	end

	
	return o

end