-- *************************************
-- *                                   *
-- *                Map                *
-- *                             Class *
-- *************************************
mapClass = {};function mapClass:new(o)
	
    -- Constructor
	local function constructor(o)
	
		o = o or {}
		
		-- Base properties	
		o.MAP_ROTATION = {math.rad(90), 0, 0}
		o.MAP_SCALE = {0.05, 0.05, 0.05}		
		
		-- Nested properties
		o.map = {} 

		-- Set metatable
		setmetatable(o, self)    
		self.__index = self

		
		return self

	end;self = constructor(o)

	-- *************************************
	-- *              Methods              *
	-- *************************************	
	-- Initialize map
	function self:init(script_path)
		
		-- Load map script
		local map_script = self:load(script_path)

		-- Create map from loaded script
		self:create(map_script)		

	end

	-- Load map script
	function self:load(script_path)

		-- Load script data and return it
		return require(script_path)

	end

	-- Create map from script
	function self:create(map)

		-- Loop through map elements
		for i, value in ipairs(map.models) do

			-- Load texture
			local mapTexture = love.graphics.newImage("assets/stage/"..value.textureName)

			-- Filter texture to pixellize
			mapTexture:setFilter("nearest", "nearest",1)

			-- Create object to set keys
			self.map[i] = {}

			-- Load and texture 3D model			
			self.map[i].model = self.lib.newModel("assets/stage/"..value.objectName, 
				mapTexture, nil, 
				self.MAP_ROTATION, self.MAP_SCALE
			)

			-- Set culling parameter
			self.map[i].culling = value.culling

		end

	end

	-- Render map
	function self:render()

		-- Loop through map elements and drawn them
		for i=1, #self.map do

			-- Set map element culling
			love.graphics.setMeshCullMode(self.map[i].culling)

			-- Draw map element
			self.map[i].model:draw()

		end

	end

	
	return o

end