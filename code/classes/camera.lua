-- *************************************
-- *                                   *
-- *              Camera               *
-- *                             Class *
-- *************************************
cameraClass = {};function cameraClass:new(o)
	
    -- Constructor
	local function constructor(o)
	
		o = o or {}
		
		-- Base properties			
		o.savedMousePosition = {
			x = 0,
			y = 0
		}
		o.fov = 50
		o.rotationSpeed = 0.3 
		o.zoomSpeed = 0.1
		o.movementSpeed = 0.8
		o.pivotPosition = {
			x = -70,
			y = 40,
			z = 70
		}

		o.pivotAngle = {
			x = math.pi,
			y = 0,
		} 
		o.pivotDistance = 100

		-- Nested properties  
		o.position = {
			x=-150,
			y=40,
			z=50
		}

		o.angle = {
			x=0,
			y=0,
			z=0,
			pitch = 0 
		}
		
		-- Set metatable
		setmetatable(o, self)    
		self.__index = self

		
		return self

	end;self = constructor(o)

	-- *************************************
	-- *              Methods              *
	-- *************************************	
	-- Initialize camera
	function self:init()

		-- Set camera field of view
		self.lib.camera.fov = math.rad(self.fov)

		-- Update view matrix with current setup
		self.lib.camera.updateViewMatrix()

		-- Update projection matrix with current setup
		self.lib.camera.updateProjectionMatrix()

	end

	-- Update camera view
	function self:updateView()

		-- Update camera look in direction 
		self.lib.camera.lookInDirection(
			self.position.x, 
			self.position.z, 
			self.position.y, 
			self.angle.y, 
			self.angle.pitch
		)
		self.lib.camera.fov = math.rad(self.fov)
		-- Update projection matrix with current setup
		self.lib.camera.updateProjectionMatrix()

	end

	-- Update camera control
	function self:updateControls(dt)

		if love.keyboard.isDown("ralt") or love.keyboard.isDown("lalt")then

			-- mouse delta x and y 
			local mouseX, mouseY = love.mouse.getPosition()
			local xDiff = mouseX - self.savedMousePosition.x 
			local yDiff = mouseY - self.savedMousePosition.y
			if love.mouse.isDown(1) then

				-- Update camera rotation
				self:updateRotation(xDiff, yDiff)

			elseif love.mouse.isDown(2) then

				--Update camera zoom
				self:updateZoom(yDiff)

			end

			-- set the mouse at the center of the screen 
			local width, height = love.graphics.getDimensions()
			if mouseX >= width - 50 or
				 mouseX <= 50 or
				 mouseY >= height - 50 or
				 mouseY <= 50 then
					 
				love.mouse.setPosition(width / 2, height / 2)
				mouseX = width / 2
				mouseY = height / 2
			end

			self.savedMousePosition.x = mouseX
			self.savedMousePosition.y = mouseY
		end
		
		-- Update pivot y-axis
		self:updateHeight(dt)

		-- Update Camera position
		self:updatePosition()
	end

	-- Update camera rotation
	function self:updateRotation(x, y)
		self.pivotAngle.x = (o.pivotAngle.x - 
							math.rad(x / 2 * self.rotationSpeed))
							% (2 * math.pi)

		self.pivotAngle.y = (o.pivotAngle.y + 
							math.rad(y / 2 * self.rotationSpeed)) 
							% (2 * math.pi)
	end

	--Update camera zoom
	function self:updateZoom(y)
		if (y > 0 and self.fov > 3 ) or 
		   (y < 0 and self.fov < 140 ) then

   			camera.fov = (camera.fov - y  * self.zoomSpeed) 

		end
		
	end

	-- Update camera pivot y-axis
	function self:updateHeight(dt)
		if love.keyboard.isDown("space") then
			self.pivotPosition.y = self.pivotPosition.y + 50 * self.movementSpeed * dt
		elseif love.keyboard.isDown("lshift") then
			self.pivotPosition.y = self.pivotPosition.y - 50 * self.movementSpeed * dt
		end
	end

	-- Update camera position
	function self:updatePosition()
		self.position.x = math.cos(self.pivotAngle.x) *
						  math.cos(self.pivotAngle.y) * 
						  self.pivotDistance + self.pivotPosition.x

		self.position.z = math.sin(self.pivotAngle.x) * 
						  math.cos(self.pivotAngle.y) * 
						  self.pivotDistance + self.pivotPosition.z

		self.position.y = math.sin(self.pivotAngle.y) * 
						  self.pivotDistance + self.pivotPosition.y

		self.angle.y = self.pivotAngle.x 
		self.angle.pitch = self.pivotAngle.y - math.pi
	end

	return o 

end