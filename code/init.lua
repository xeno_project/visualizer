-- *************************************
-- *                                   *
-- *            Visualizer             *
-- *                          By Xoxyi *
-- *************************************

-- Game identity
love.filesystem.setIdentity("xeno-visualizer")

-- Controls
love.keyboard.setKeyRepeat(true)

-- Mouse
love.mouse.setVisible(false)

-- Filters
love.graphics.setDefaultFilter("nearest", "nearest", 1)
love.graphics.setLineStyle("rough")

-- Fonts
font = love.graphics.newImageFont("assets/fonts/font.png",
" abcdefghijklmnopqrstuvwxyz" ..
"ABCDEFGHIJKLMNOPQRSTUVWXYZ0" ..
"123456789.,!?-+/():;%&`'*#=[]\"")	
font:setLineHeight(0.88)

font2 = love.graphics.newImageFont("assets/fonts/font2.png",
" abcdefghijklmnopqrstuvwxyz" ..
"ABCDEFGHIJKLMNOPQRSTUVWXYZ0" ..
"123456789.,!?-+/():;%&`'*#=[]\"")	

font3 = love.graphics.newImageFont("assets/fonts/font3.png",
" abcdefghijklmnopqrstuvwxyz" ..
"ABCDEFGHIJKLMNOPQRSTUVWXYZ0" ..
"123456789.,!?-+/():;%&`'*#=[]\"")	
    
-- Set font
love.graphics.setFont(font)
    
-- Avoid blur/smoothing
font:setFilter( "nearest", "nearest" )
font2:setFilter( "nearest", "nearest" )

-- Canvas creation
worldCanvas = love.graphics.newCanvas(320, 240)
worldCanvas:setFilter("nearest", "nearest", 1)

compassShadowCanvas = love.graphics.newCanvas(640, 480)
compassShadowCanvas:setFilter("nearest", "nearest", 1)	

compassStructureCanvas = love.graphics.newCanvas(640, 480)
compassStructureCanvas:setFilter("nearest", "nearest", 1)

compassOrientationCanvas = love.graphics.newCanvas(640, 480)
compassOrientationCanvas:setFilter("nearest", "nearest", 1)	

compassCardinalsCanvas = love.graphics.newCanvas(640, 480)
compassCardinalsCanvas:setFilter("nearest", "nearest", 1)	