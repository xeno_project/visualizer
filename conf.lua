function love.conf(c)

	-- window
	c.window.title = "Xeno Visualizer V0.1"
	c.window.resizable = false

	-- screen
	c.window.width = 800
	c.window.height = 600
	c.window.depth = 24
	c.window.vsync = true
	c.window.fullscreen = false
	c.window.highdpi = false
	c.window.msaa = 0
	c.window.stencil = 8 
	c.window.usedpiscale = false
	
	-- modules
	c.modules.audio = true              -- Enable the audio module (boolean)
	c.modules.event = true              -- Enable the event module (boolean)
	c.modules.graphics = true           -- Enable the graphics module (boolean)
	c.modules.image = true              -- Enable the image module (boolean)
	c.modules.joystick = true           -- Enable the joystick module (boolean)
	c.modules.keyboard = true           -- Enable the keyboard module (boolean)
	c.modules.math = true               -- Enable the math module (boolean)
	c.modules.mouse = true              -- Enable the mouse module (boolean)
	c.modules.physics = false           -- Enable the physics module (boolean)
	c.modules.sound = true              -- Enable the sound module (boolean)
	c.modules.system = true             -- Enable the system module (boolean)
	c.modules.timer = true              -- Enable the timer module (boolean)
	c.modules.touch = false             -- Enable the touch module (boolean)
	c.modules.video = true              -- Enable the video module (boolean)
	c.modules.window = true             -- Enable the window module (boolean)
	c.modules.thread = false            -- Enable the thread module (boolean)	

end