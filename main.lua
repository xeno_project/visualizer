-- *************************************
-- *                                   *
-- *          Xeno visualizer          *
-- *                          By Xoxyi *
-- *************************************
-- Includes 
local g3d = require "code/libraries/g3d"
local enabledCameraInfo = false
require "code/init"
require "code/classes/debug"
require "code/classes/camera"
require "code/classes/map"
require "code/classes/gear"

-- *************************************
-- *                                   *
-- *           Load callback           *
-- *                                   *
-- *************************************
function love.load(arg)

  --comment
	-- Instanciate debug class
	debug = debugClass:new{}	 

	-- Instanciate camera class
	camera = cameraClass:new{lib = g3d};camera:init()

  -- Instanciate map class
  map = mapClass:new{lib = g3d};map:init("code/data/maps/sr_1")

  -- Instanciate gear class
  gear = gearClass:new{lib = g3d};gear:init("gear.obj", "gear.png", "Assets")


end


-- *************************************
-- *                                   *
-- *          Update callback          *
-- *                                   *
-- *************************************
function love.update(dt)

  camera:updateView()

  camera:updateControls(dt)

end


-- *************************************
-- *                                   *
-- *           Draw callback           *
-- *                                   *
-- *************************************
function love.draw()

  map:render()
  
  gear:render()

  if enabledCameraInfo then debug:displayCameraData(camera, 10, 10) end

end


-- *************************************
-- *         Control callbacks         *
-- *************************************
-- Keypress callbacks
function love.keypressed(key,scancode,isrepeat)

  -- Toggle debug data
  if key == "z" then
    enabledCameraInfo = not enabledCameraInfo
  end
end

-- Mousepressed callbacks
function love.mousepressed(x, y, button, istouch)
  if button == 1 or button == 2 then
    camera.savedMousePosition.x = x
    camera.savedMousePosition.y = y
  end
end

function love.wheelmoved(x, y)
  if love.keyboard.isDown("ralt") or love.keyboard.isDown("lalt") then
    if y == -1  and camera.pivotDistance > 10 then
      camera.pivotDistance = (camera.pivotDistance - 5 * camera.movementSpeed) 
    
    elseif y == 1 and camera.pivotDistance < 200 then
      camera.pivotDistance = (camera.pivotDistance + 5 * camera.movementSpeed) 
    end
  end
end


